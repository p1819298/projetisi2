# AnimalsHub - Banque d'images pour animaux
### Caractéristiques principales
- Site créé à l'aide de Laravel
- MariaDB
- Bootstrap
### Procédure d'installation
Pour réutiliser notre magnifique projet depuis chez vous, il faut:
1. Créer un projet Laravel afin d'avoir les éléments
qui ne sont pas dans le git tel que le dossier vendor ou le fichier .env
2. Récupérer tous les éléments du git
3. Configurer le fichier .env pour que le site soit connecté à votre base de données
4. Donner les droits nécessaires. Attention ! Ne pas oublier de donner les droits 775 au dossier public
5. Faire un "Artisan: Migrate" pour créer les tables
6. Exécuter dans cet ordre les commandes suivantes dans le terminal (afin de remplir les tables) :
```sh
  sudo /usr/bin/php artisan db:seed --class=GenreSeeder

  sudo /usr/bin/php artisan db:seed --class=UserSeeder

  sudo /usr/bin/php artisan db:seed --class=ImageSeeder
```
7. Créer un compte utilisateur sur http://localhost/web/VOTRE_PROJET/public/register
8. Vous pouvez tester toutes les fonctionnalités du site!
