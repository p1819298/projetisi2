<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;

class CommentaireFormController extends Controller
{
    public function postInfos(Request $request)
    {
        $utilisateurid = Auth::user()->id;
        $imageid = $request->input('image_id');
        $commentaire = $request->input('commentaire');

        if($request->input('commentaire') == "")
        {
            return redirect('images/'.$imageid);
        }

        Comment::addComment($utilisateurid, $imageid, $commentaire);
        return redirect('images/'.$imageid);
    }

    public function getInfos()
    {
        return redirect('images');
    }
}
