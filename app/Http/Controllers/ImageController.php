<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchImageRequest;
use App\Http\Requests\InsertImageRequest;
use App\Http\Requests\UpdateImageRequest;
use App\Models\Image;

use App\Models\Genre;

use App\Models\image_utilisateur;
use App\Models\User;
use App\Models\Comment;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;

class ImageController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::guest()){
            return redirect()->route('login');
        }

        $images = Image::all();
        return view('homeConnected',compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listeGenre = Genre::all();
        $genre_id = 1;
        return view('createImage', compact('listeGenre','genre_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreImageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsertImageRequest $request)
    {
        $chemin_dest = "assets/img/contenu_site/";
        $fichier = $chemin_dest.basename($_FILES["source"]["name"]);
        move_uploaded_file($_FILES["source"]["tmp_name"], $fichier);

        $utilisateur_id = Auth::user()->id;
        Image::create(array_merge($request->all(), ['user_id' => $utilisateur_id],['date_creation' => date("y-m-d")],['source' => $fichier]));

        return view('confirmImageAdded');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    /*public function show(Image $image)
    {

        return view('showImageInfo', compact('image'));
    }*/

    public function show($image_id)
    {
        $utilisateur_id = Auth::user()->id;

        $image = Image::where('id', $image_id)->first();
        $image_liked_state = image_utilisateur::existeLine($utilisateur_id, $image_id);
        $path = asset($image->source);
        $commentaires = Comment::getComment($image->id);

        return view('showImageInfo', compact('image', 'path', 'image_liked_state', 'commentaires'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        $listeGenre = Genre::all();
        return view('modifyImage',compact('image','listeGenre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateImageRequest  $request
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateImageRequest $request, Image $image)
    {
        echo $image->source;
        if ($_FILES["source"]["name"]!=NULL)
        {
            $chemin_dest = "assets/img/contenu_site/";
            $fichier = $chemin_dest.basename($_FILES["source"]["name"]);
            move_uploaded_file($_FILES["source"]["tmp_name"], $fichier);
        }
        else
        {
            $fichier = old('source',$image->source);
        }

        $image->update(array_merge($request->all(), ['utilisateur_id' => Auth::user()->id],['date_creation' => date("ymd")],['source' => $fichier]));
        return back()->with('info','Votre image a bien été modifiée !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        $image->delete();
        return back()->with('info','Votre image a bien été supprimée !');
    }
}
