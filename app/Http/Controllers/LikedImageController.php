<?php

namespace App\Http\Controllers;

use App\Models\image_utilisateur;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\UtilisateurController;
use Illuminate\Support\Facades\Auth;

class LikedImageController extends Controller
{
    public function postInfos(Request $request)
    {
        $utilisateur_id = Auth::user()->id;
        $image_id = $request->input('image_id');

        $imageisLikedByUser = image_utilisateur::existeLine($utilisateur_id, $image_id);
        if($imageisLikedByUser == true)
        {
            echo 'deja like, on detruit';
            image_utilisateur::destroyImageUtilisateur($utilisateur_id, $image_id);
        }
        else
        {
            echo 'paslike, on ajoute';
            image_utilisateur::addImageUtilisateur($utilisateur_id, $image_id);
        }

        return redirect('images/'.$image_id);
    }

    public function getInfos()
    {
        return redirect('images');
    }


}
