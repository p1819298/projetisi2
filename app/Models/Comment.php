<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = ['image_id', 'user_id', 'commentaire', 'created_at', 'updated_at'];
    public $timestamps = false;

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function addComment( $utilisateurid , $imageid ,  $commentaire )
    {
        Comment::create(
            [
                'image_id' => $imageid,
                'user_id' => $utilisateurid,
                'commentaire' => $commentaire,
                'created_at' => date("Ymd"),
                'updated_at' => date("Ymd")
            ]
        );
    }

    public static function getComment($imageid)
    {
        $image = Image::find($imageid);
        $image->commentaires()->get();

        $commentaires = $image->commentaires;

        return $commentaires;
    }
}
