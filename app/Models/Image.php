<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
    protected $fillable = ['title','description','user_id','id_genre','source','date_creation'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public static function getImagesWithTitle(String $str)
    {
    }

    public function utilisateurs()
    {
        return $this->belongsToMany(User::class);
    }

    public function commentaires()
    {
        return $this->hasMany(Comment::class);
    }

    /** Permet de savoir si une image est liké par un utilisateur précis */
    public static function getIfImageisLiked($imageid,$utilisateur_id)
    {
        //$result_request = image_utilisateur::select('id')->where([['image_id','=',$image_id],['utilisateur_id','=', $utilisateur_id]])->get()
        $image = Image::where('id', $imageid)->first();
        $result_request = $image->utilisateurs();

        foreach($result_request as $utilisateur)
        {

        }
        echo $result_request[0];

/*
        if(count($result_request) != 0)
        {
            return true;
        }
        else
        {
            return false;
        }*/
    }
    
    
}
