<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Genre::create([
            'id' => 1,
            'genreName' => "Félins"
        ]);

        \App\Models\Genre::create([
            'id' => 2,
            'genreName' => "Réptiles"
        ]);

        \App\Models\Genre::create([
            'id' => 3,
            'genreName' => "Oiseaux"
        ]);

        \App\Models\Genre::create([
            'id' => 4,
            'genreName' => "Poissons"
        ]);

        \App\Models\Genre::create([
            'id' => 5,
            'genreName' => "Animaux de la ferme"
        ]);


    }
}
