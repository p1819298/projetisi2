@extends('layouts.layout')

@section('title')
    Image ajoutée avec succès
@endsection

@section('correction_url')../@endsection

@section('page_contenu')
<div class="alert alert-success text-center w-50 mx-auto mt-5" role="alert">
    
  Votre image a été ajoutée avec succès !
  <a href="{{route('personalInfo.show',Auth::user()->id)}}" class="">Retour à votre page</a>
  
</div>

<div style="width:480px" class="text-center mx-auto"><iframe allow="fullscreen" frameBorder="0" height="400" src="https://giphy.com/embed/qm4OiHF2wGoq7onwrn/video" width="480"></iframe></div>

@endsection