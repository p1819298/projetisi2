@extends('layouts.layout')

@section('title')
    Ajouter une image
@endsection

@section('correction_url')../@endsection

@section('page_contenu')
<div class="container">
    <div class="row shadow card text-black w-50 mx-auto p-3" style="background-color:rgb(27,30,31);">
        <p class="text-center display-5">Publier une image</p>  
        <form action="{{route('images.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group mt-4">
                <label for="title" class="form-label col-md-5 col-10">Titre de l'image</label>
                <input id="title" class="border form-control col-md-3 text-light shadow col-6"  name="title" style="background-color:rgb(24,26,27);border:1px solid rgb(50,54,56);" type="text" placeholder="Mon animal preferé"/>
                @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group row mt-4">
                <label for="description" class="form-label col-md-5 col-10">Description</label>
                <textarea id="description" class="border form-control text-light col-md-3 shadow col-6" style="background-color:rgb(24,26,27);"  name="description" type="textarea" placeholder="Je l'aime tellement"></textarea>
                @error('description')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group row mt-4">
            <label for="id_genre" class="form-label col-md-5 col-10">Genre</label>
            <select name="id_genre" name="id_genre" id="id_genre" style="background-color:rgb(24,26,27);" class="form-select text-light">
            @foreach($listeGenre as $genre)
                    <option class="text-light" value="{{$genre->id}}" {{ ( $genre->id == $genre_id) ? 'selected' : '' }}>{{$genre->genreName}}</option>
            @endforeach
            </select>
            
            </div>

            <div class="form-group row mt-4">
                <label for="source" name="source" class="label-file" >Choisir une image</label>
                <input id="source" name="source" class="input-file mt-3 text-light" type="file">
                @error('source')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="justify-content-end mt-4 text-center">
                <button type="submit" class="btn btn-success">Publier !</button>
            </div>
        </form>
    </div>
</div>
@endsection

