@extends('layouts.layout')

@section('title')
    Home
@endsection

@section('page_contenu')

<?php $i=1; ?>
<?php $nbImages=0;?>
<div class="pin_container">

@foreach($images as $image)
<?php
    
    if($i==1)
    {
        
        echo '<a href="'.route('images.show',$image->id).'" data="'.$nbImages.'" class="card card_small">';
        $i = $i+1;
    }
    else if($i==2)
    {
        echo '<a href="'.route('images.show',$image->id).'" data="'.$nbImages.'" class="card card_medium">';
        $i = $i+1;
    }
    else
    {
        echo '<a href="'.route('images.show',$image->id).'" data="'.$nbImages.'" class="card card_large">';
        $i = 1;
    }
    
    
?>
    <img class="card-img-top rounded" width="400" height="500" src="{{ asset($image->source) }}" alt="">
    <?php $nbImages = $nbImages+1?>
    
</a>
@endforeach
</div>  

@endsection

@section('script')
/*var classname = document.getElementsByClassName("card");
var classbutton = document.getElementsByClassName("bouton_aimer");

var visible = function() {
    var attribute = this.getAttribute("data");
    var button = document.getElementById("bouton"+attribute);
    button.style.visibility = 'visible';
};

var cacher = function() {
    var attribute = this.getAttribute("data");
    var button = document.getElementById("bouton"+attribute);
    button.style.visibility = 'hidden';
};


for (var i = 0; i < classname.length; i++) {
    classname[i].addEventListener('mouseover', visible, false);
}

for (var i = 0; i < classname.length; i++) {
    classname[i].addEventListener('mouseout', cacher, false);
}*/
@endsection