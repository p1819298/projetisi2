<nav class="navbar navbar-dark navbar-expand-lg bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><img width="50" height="50" class="rounded-circle" src="{{asset('assets/img/iconesPdp/logo2.png')}}"/></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="{{route('images.index')}}">Accueil</a>
            </li>
            <!--<li class="nav-item">
                <a class="nav-link" href="#">Aujourd'hui</a>
            </li>-->
        </ul>
        <form action="@yield('correction_url')search_image" method="post" class="d-flex" role="search" style="width: 100%;">
            @csrf    
            <div class="input-group" style="width: 90%;margin-left:5%;">
                <input value="" type="text" style="background-color:rgb(24,26,27);border-color:rgb(60,65,68);color:white"  name="search_image" id="search_image" class="form-control" placeholder="Rechercher" aria-label="Username" aria-describedby="basic-addon1">
                <button type="submit" class="input-group-text" id="basic-addon1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                   </svg>
                </button>
            </div>
        </form>
        <ul class="navbar-nav navbar-nav-scroll" style="--bs-scroll-height: 100px;">
            <li class="nav-item">
                <!--<a class="nav-link" aria-current="page" href="#">Deconnexion</a>-->
                @include('layouts.user.deconnexion')
            </li>
            <li class="nav-item">
                <a class="nav-link text-dark btn btn-light" style=" border-radius: 50%;width: 40px;" href="{{route('personalInfo.show',Auth::user()->id)}}">{{ Auth::user()->prenom[0] }}</a>

            </li>
        </ul>
    </div>
  </div>
</nav>