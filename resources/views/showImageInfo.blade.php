@extends('layouts.layout')

@section('page_titre')
{{$image->title}}
@endsection


@section('correction_url')../@endsection

@section('page_contenu')
    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col-2">
                <a href="{{url()->previous()}}">
                <button type="button" class="btn btn-primary">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
                    </svg>
                    Retour
                </button>
                </a>
            </div>
            <div class="col-8 bg-dark border border-rounded border-2" id="contenu_site">
                <div class="row">
                    <div class="col-6 p-0">
                        <img class="contenu_site_img" src="{{$path}}"/>
                    </div>
                    <div class="col-6">
                        <form method="post" action="../liked_image" >
                            @csrf
                            <input name="image_id" id="image_id" type="hidden" value="{{$image->id}}" />
                            <button type="submit" class="btn btn-danger mt-3">
                                @if($image_liked_state == false)
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                    <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                </svg>
                                @else
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart-fill" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                                </svg>
                                @endif
                            </button>
                        </form> 
                        <h1><strong><?php echo $image->title; ?></strong></h1>
                        <p>{{$image->description}}</p>
                        <p>Créateur : <a href="{{route('personalInfo.show',$image->user_id)}}" class="text-white">{{$image->user->nom}} {{$image->user->prenom}}</a></p>
                        <h4><?php 
                            $nb_commentaires = count($commentaires);
                            if($nb_commentaires >= 2)
                            {
                                echo $nb_commentaires." commentaires";
                            }
                            else
                            {
                                echo $nb_commentaires." commentaire";
                            } ?></h4>
                        <p>
                        @foreach($commentaires as $commentaire)
                            <strong>{{$commentaire->user->nom}} {{$commentaire->user->prenom}}</strong>  {{$commentaire->commentaire}}<br/>
                        @endforeach
                        </p>
                        <!--<p>//Mettre les commentaires ici</p>-->
                        
                        <form action="../post_comment" method="post">
                            @csrf
                            <input name="image_id" id="image_id" type="hidden" value="{{$image->id}}" />
                            <div class="form-floating">
                                <textarea class="form-control" placeholder="Leave a comment here" id="commentaire", name="commentaire"></textarea>
                                <label for="floatingTextarea" style="color:gray">Ajouter un commentaire</label>
                            </div>
                            <div class="mt-2" align="right">
                                <button type="submit" class="btn btn-primary mb-3">Terminé</button>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection