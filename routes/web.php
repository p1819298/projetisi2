<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\SearchImageController;
use App\Http\Controllers\LikedImageController;
use App\Http\Requests\SearchImageRequest;
use App\Http\Controllers\UtilisateurController;
use App\Http\Controllers\CommentaireFormController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


/** Permet de faire fonctionner la barre de recherche  */
Route::get('search_image',[SearchImageController::class, 'getInfos']);
Route::post('search_image',[SearchImageController::class, 'postInfos']);

/** Permet de faire aimer une image  */
Route::get('liked_image',[LikedImageController::class, 'getInfos']);
Route::post('liked_image',[LikedImageController::class, 'postInfos']);

/** Permet de mettre des commentaires sur des images */
Route::get('post_comment',[CommentaireFormController::class, 'getInfos']);
Route::post('post_comment',[CommentaireFormController::class, 'postInfos']);

/** Page d'accueil de quelqu'un qui se connecte */
Route::resource('images',ImageController::class);
Route::resource('personalInfo', UtilisateurController::class);






Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

